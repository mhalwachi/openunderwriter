CREATE DATABASE IF NOT EXISTS ##dbname.openunderwriter## character set utf8;
GRANT ALL ON ##dbname.openunderwriter##.* TO '##dbusername##'@'localhost' IDENTIFIED BY '##dbpassword##' WITH GRANT OPTION;
GRANT ALL ON ##dbname.openunderwriter##.* TO '##dbusername##'@'localhost.localdomain' IDENTIFIED BY '##dbpassword##' WITH GRANT OPTION;

USE ##dbname.openunderwriter##;

-- Baseline OpenUnderwriter 3.X Schema Definition.
--
-- DO NOT MODIFY ANYTHING BEYOND THIS POINT!! All database modifications MUST 
-- be performed by liquibase scripts. The content of this file will only change
-- as part of a major-version OpenUnderwriter upgrade.

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ashAssessmentSheet`
--

DROP TABLE IF EXISTS `ashAssessmentSheet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ashAssessmentSheet` (
  `ashUID` bigint(20) NOT NULL,
  `ashAttribute` longtext,
  `ashCreatedDate` datetime DEFAULT NULL,
  `ashExternalSystemId` varchar(255) NOT NULL,
  `ashForeignSystemId` varchar(255) DEFAULT NULL,
  `ashLock` tinyint(1) NOT NULL,
  `ashSerialVersion` bigint(20) NOT NULL,
  `ashUpdatedDate` datetime DEFAULT NULL,
  `ashAssessmentLines` longtext,
  `ashAutoPriority` int(11) NOT NULL,
  `ashProcessedOrderCounter` int(11) NOT NULL,
  `ashCreatedBy` bigint(20) DEFAULT NULL,
  `ashUpdatedBy` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ashUID`),
  UNIQUE KEY `ashExternalSystemId` (`ashExternalSystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `assAsset`
--

DROP TABLE IF EXISTS `assAsset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assAsset` (
  `assUID` bigint(20) NOT NULL,
  `assAttribute` longtext,
  `assCreatedBy` bigint(20) DEFAULT NULL,
  `assCreatedDate` datetime DEFAULT NULL,
  `assExternalSystemId` varchar(255) NOT NULL,
  `assForeignSystemId` varchar(255) DEFAULT NULL,
  `assLock` bit(1) DEFAULT NULL,
  `assSerialVersion` bigint(20) NOT NULL,
  `assUpdatedBy` bigint(20) DEFAULT NULL,
  `assUpdatedDate` datetime DEFAULT NULL,
  `assAssetTypeId` varchar(255) DEFAULT NULL,
  `assId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`assUID`),
  UNIQUE KEY `assExternalSystemId` (`assExternalSystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conConfig`
--

DROP TABLE IF EXISTS `conConfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conConfig` (
  `namespace` varchar(255) NOT NULL,
  `manager` varchar(255) NOT NULL,
  `configuration` longblob NOT NULL,
  `validfrom` bigint(20) NOT NULL,
  `validto` bigint(20) DEFAULT NULL,
  `who` varchar(32) DEFAULT NULL,
  `version` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `covCoverage`
--

DROP TABLE IF EXISTS `covCoverage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `covCoverage` (
  `covUID` bigint(20) NOT NULL,
  `covAttribute` longtext,
  `covCreatedBy` bigint(20) DEFAULT NULL,
  `covCreatedDate` datetime DEFAULT NULL,
  `covExternalSystemId` varchar(255) NOT NULL,
  `covForeignSystemId` varchar(255) DEFAULT NULL,
  `covLock` bit(1) DEFAULT NULL,
  `covSerialVersion` bigint(20) NOT NULL,
  `covUpdatedBy` bigint(20) DEFAULT NULL,
  `covUpdatedDate` datetime DEFAULT NULL,
  `covCoverageTypeId` varchar(255) DEFAULT NULL,
  `covDeductibleAmount` decimal(19,2) DEFAULT NULL,
  `covDeductibleCurrency` varchar(255) DEFAULT NULL,
  `covDescription` varchar(255) DEFAULT NULL,
  `covEffectiveDate` datetime DEFAULT NULL,
  `covEnabled` bit(1) NOT NULL,
  `covExpiryDate` datetime DEFAULT NULL,
  `covId` varchar(255) DEFAULT NULL,
  `covLimitAmount` decimal(19,2) DEFAULT NULL,
  `covLimitCurrency` varchar(255) DEFAULT NULL,
  `covName` varchar(255) DEFAULT NULL,
  `covOptional` bit(1) NOT NULL,
  `covBrokerUIDpar` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`covUID`),
  UNIQUE KEY `covExternalSystemId` (`covExternalSystemId`),
  KEY `FKC6B73192BA6F347E` (`covBrokerUIDpar`),
  CONSTRAINT `FKC6B73192BA6F347E` FOREIGN KEY (`covBrokerUIDpar`) REFERENCES `parParty` (`parUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dcoDocumentContent`
--

DROP TABLE IF EXISTS `dcoDocumentContent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dcoDocumentContent` (
  `dcoUID` bigint(20) NOT NULL,
  `dcoAttribute` longtext,
  `dcoCreatedDate` datetime DEFAULT NULL,
  `dcoExternalSystemId` varchar(255) NOT NULL,
  `dcoForeignSystemId` varchar(255) DEFAULT NULL,
  `dcoLock` tinyint(1) NOT NULL,
  `dcoSerialVersion` bigint(20) NOT NULL,
  `dcoUpdatedDate` datetime DEFAULT NULL,
  `dcoInTableContent` longblob,
  `dcoDocumentUIDdoc` bigint(20) DEFAULT NULL,
  `dcoProductTypeId` varchar(255) DEFAULT NULL,
  `dcoCreatedBy` bigint(20) DEFAULT NULL,
  `dcoUpdatedBy` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dcoUID`),
  UNIQUE KEY `dcoExternalSystemId` (`dcoExternalSystemId`),
  KEY `FK80F4B0EE5E88B029` (`dcoDocumentUIDdoc`),
  CONSTRAINT `FK80F4B0EE5E88B029` FOREIGN KEY (`dcoDocumentUIDdoc`) REFERENCES `docDocument` (`docUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `docDocument`
--

DROP TABLE IF EXISTS `docDocument`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docDocument` (
  `docUID` bigint(20) NOT NULL,
  `docAttribute` longtext,
  `docCreatedDate` datetime DEFAULT NULL,
  `docExternalSystemId` varchar(255) NOT NULL,
  `docForeignSystemId` varchar(255) DEFAULT NULL,
  `docLock` tinyint(1) NOT NULL,
  `docSerialVersion` bigint(20) NOT NULL,
  `docUpdatedDate` datetime DEFAULT NULL,
  `docDescription` longtext,
  `docFileName` varchar(255) DEFAULT NULL,
  `docMimeType` varchar(255) DEFAULT NULL,
  `docOtherType` varchar(255) DEFAULT NULL,
  `docTitle` varchar(255) DEFAULT NULL,
  `docType` varchar(255) DEFAULT NULL,
  `docCreatedBy` bigint(20) DEFAULT NULL,
  `docUpdatedBy` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`docUID`),
  UNIQUE KEY `docExternalSystemId` (`docExternalSystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dreDocumentRequest`
--

DROP TABLE IF EXISTS `dreDocumentRequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dreDocumentRequest` (
  `dreUID` bigint(20) NOT NULL,
  `dreAttribute` longtext,
  `dreCreatedDate` datetime DEFAULT NULL,
  `dreExternalSystemId` varchar(255) NOT NULL,
  `dreForeignSystemId` varchar(255) DEFAULT NULL,
  `dreLock` tinyint(1) NOT NULL,
  `dreSerialVersion` bigint(20) NOT NULL,
  `dreUpdatedDate` datetime DEFAULT NULL,
  `dreDocumentType` varchar(255) DEFAULT NULL,
  `dreDocumentUID` bigint(20) DEFAULT NULL,
  `dreRequestId` varchar(255) DEFAULT NULL,
  `dreRequestType` varchar(255) DEFAULT NULL,
  `dreSourceUID` bigint(20) DEFAULT NULL,
  `dreCreatedBy` bigint(20) DEFAULT NULL,
  `dreUpdatedBy` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dreUID`),
  UNIQUE KEY `dreExternalSystemId` (`dreExternalSystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fsrForeignSystemReference`
--

DROP TABLE IF EXISTS `fsrForeignSystemReference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fsrForeignSystemReference` (
  `fsrUID` bigint(20) NOT NULL,
  `fsrAttribute` longtext,
  `fsrCreatedDate` datetime DEFAULT NULL,
  `fsrExternalSystemId` varchar(255) NOT NULL,
  `fsrForeignSystemId` varchar(255) DEFAULT NULL,
  `fsrLock` bit(1) DEFAULT NULL,
  `fsrSerialVersion` bigint(20) NOT NULL,
  `fsrUpdatedDate` datetime DEFAULT NULL,
  `fsrReference` varchar(255) DEFAULT NULL,
  `fsrType` varchar(255) DEFAULT NULL,
  `fsrCreatedBy` bigint(20) DEFAULT NULL,
  `fsrUpdatedBy` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`fsrUID`),
  UNIQUE KEY `fsrExternalSystemId` (`fsrExternalSystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jAssAssAss`
--

DROP TABLE IF EXISTS `jAssAssAss`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jAssAssAss` (
  `UIDass` bigint(20) NOT NULL,
  `assetUIDass` bigint(20) NOT NULL,
  UNIQUE KEY `assetUIDass` (`assetUIDass`),
  KEY `FKAED1A4D7C30A5D40` (`assetUIDass`),
  KEY `FKAED1A4D7C7D6650` (`UIDass`),
  CONSTRAINT `FKAED1A4D7C30A5D40` FOREIGN KEY (`assetUIDass`) REFERENCES `assAsset` (`assUID`),
  CONSTRAINT `FKAED1A4D7C7D6650` FOREIGN KEY (`UIDass`) REFERENCES `assAsset` (`assUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jDcoFsrFsr`
--

DROP TABLE IF EXISTS `jDcoFsrFsr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jDcoFsrFsr` (
  `UIDdco` bigint(20) NOT NULL,
  `foreignSystemReferenceUIDfsr` bigint(20) NOT NULL,
  PRIMARY KEY (`UIDdco`,`foreignSystemReferenceUIDfsr`),
  UNIQUE KEY `foreignSystemReferenceUIDfsr` (`foreignSystemReferenceUIDfsr`),
  KEY `FK227B106693BC173D` (`foreignSystemReferenceUIDfsr`),
  KEY `FK227B1066E2808C83` (`UIDdco`),
  CONSTRAINT `FK227B106693BC173D` FOREIGN KEY (`foreignSystemReferenceUIDfsr`) REFERENCES `fsrForeignSystemReference` (`fsrUID`),
  CONSTRAINT `FK227B1066E2808C83` FOREIGN KEY (`UIDdco`) REFERENCES `dcoDocumentContent` (`dcoUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jPolAdoDoc`
--

DROP TABLE IF EXISTS `jPolAdoDoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jPolAdoDoc` (
  `UIDpol` bigint(20) NOT NULL,
  `archivedDocumentUIDdoc` bigint(20) NOT NULL,
  UNIQUE KEY `archivedDocumentUIDdoc` (`archivedDocumentUIDdoc`),
  KEY `FK4C2EAC4F469CDCBB` (`archivedDocumentUIDdoc`),
  KEY `FK4C2EAC4F84156FE0` (`UIDpol`),
  CONSTRAINT `FK4C2EAC4F469CDCBB` FOREIGN KEY (`archivedDocumentUIDdoc`) REFERENCES `docDocument` (`docUID`),
  CONSTRAINT `FK4C2EAC4F84156FE0` FOREIGN KEY (`UIDpol`) REFERENCES `polPolicy` (`polUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jPolAslAsh`
--

DROP TABLE IF EXISTS `jPolAslAsh`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jPolAslAsh` (
  `UIDpol` bigint(20) NOT NULL,
  `assessmentSheetListUIDash` bigint(20) NOT NULL,
  `sheet_name` varchar(255) NOT NULL,
  PRIMARY KEY (`UIDpol`,`sheet_name`),
  UNIQUE KEY `assessmentSheetListUIDash` (`assessmentSheetListUIDash`),
  KEY `FK4D00A4FF51048E0D` (`assessmentSheetListUIDash`),
  KEY `FK4D00A4FF84156FE0` (`UIDpol`),
  CONSTRAINT `FK4D00A4FF51048E0D` FOREIGN KEY (`assessmentSheetListUIDash`) REFERENCES `ashAssessmentSheet` (`ashUID`),
  CONSTRAINT `FK4D00A4FF84156FE0` FOREIGN KEY (`UIDpol`) REFERENCES `polPolicy` (`polUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jPolAssAss`
--

DROP TABLE IF EXISTS `jPolAssAss`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jPolAssAss` (
  `UIDpol` bigint(20) NOT NULL,
  `assetUIDass` bigint(20) NOT NULL,
  UNIQUE KEY `assetUIDass` (`assetUIDass`),
  KEY `FK4D03D3A384156FE0` (`UIDpol`),
  KEY `FK4D03D3A3C30A5D40` (`assetUIDass`),
  CONSTRAINT `FK4D03D3A384156FE0` FOREIGN KEY (`UIDpol`) REFERENCES `polPolicy` (`polUID`),
  CONSTRAINT `FK4D03D3A3C30A5D40` FOREIGN KEY (`assetUIDass`) REFERENCES `assAsset` (`assUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jPolCovCov`
--

DROP TABLE IF EXISTS `jPolCovCov`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jPolCovCov` (
  `UIDpol` bigint(20) NOT NULL,
  `coverageUIDcov` bigint(20) NOT NULL,
  UNIQUE KEY `coverageUIDcov` (`coverageUIDcov`),
  KEY `FK50368B0384156FE0` (`UIDpol`),
  KEY `FK50368B033DED8F7B` (`coverageUIDcov`),
  CONSTRAINT `FK50368B033DED8F7B` FOREIGN KEY (`coverageUIDcov`) REFERENCES `covCoverage` (`covUID`),
  CONSTRAINT `FK50368B0384156FE0` FOREIGN KEY (`UIDpol`) REFERENCES `polPolicy` (`polUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jPolDocDoc`
--

DROP TABLE IF EXISTS `jPolDocDoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jPolDocDoc` (
  `UIDpol` bigint(20) NOT NULL,
  `documentUIDdoc` bigint(20) NOT NULL,
  UNIQUE KEY `documentUIDdoc` (`documentUIDdoc`),
  KEY `FK51E2C443AAA21859` (`documentUIDdoc`),
  KEY `FK51E2C44384156FE0` (`UIDpol`),
  CONSTRAINT `FK51E2C44384156FE0` FOREIGN KEY (`UIDpol`) REFERENCES `polPolicy` (`polUID`),
  CONSTRAINT `FK51E2C443AAA21859` FOREIGN KEY (`documentUIDdoc`) REFERENCES `docDocument` (`docUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jPolNotNot`
--

DROP TABLE IF EXISTS `jPolNotNot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jPolNotNot` (
  `UIDpol` bigint(20) NOT NULL,
  `noteUIDnot` bigint(20) NOT NULL,
  UNIQUE KEY `noteUIDnot` (`noteUIDnot`),
  KEY `FK62FB1A6384156FE0` (`UIDpol`),
  KEY `FK62FB1A63B3C35E1F` (`noteUIDnot`),
  CONSTRAINT `FK62FB1A6384156FE0` FOREIGN KEY (`UIDpol`) REFERENCES `polPolicy` (`polUID`),
  CONSTRAINT `FK62FB1A63B3C35E1F` FOREIGN KEY (`noteUIDnot`) REFERENCES `notNote` (`notUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jPolPhiPre`
--

DROP TABLE IF EXISTS `jPolPhiPre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jPolPhiPre` (
  `UIDpol` bigint(20) NOT NULL,
  `paymentHistoryUIDpre` bigint(20) NOT NULL,
  UNIQUE KEY `paymentHistoryUIDpre` (`paymentHistoryUIDpre`),
  KEY `FK65FD2ED515725D3C` (`paymentHistoryUIDpre`),
  KEY `FK65FD2ED584156FE0` (`UIDpol`),
  CONSTRAINT `FK65FD2ED515725D3C` FOREIGN KEY (`paymentHistoryUIDpre`) REFERENCES `prePaymentRecord` (`preUID`),
  CONSTRAINT `FK65FD2ED584156FE0` FOREIGN KEY (`UIDpol`) REFERENCES `polPolicy` (`polUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jPolPopPsc`
--

DROP TABLE IF EXISTS `jPolPopPsc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jPolPopPsc` (
  `UIDpol` bigint(20) NOT NULL,
  `paymentOptionUIDpsc` bigint(20) NOT NULL,
  UNIQUE KEY `paymentOptionUIDpsc` (`paymentOptionUIDpsc`),
  KEY `FK66630212223BE18C` (`paymentOptionUIDpsc`),
  KEY `FK6663021284156FE0` (`UIDpol`),
  CONSTRAINT `FK66630212223BE18C` FOREIGN KEY (`paymentOptionUIDpsc`) REFERENCES `pscPaymentSchedule` (`pscUID`),
  CONSTRAINT `FK6663021284156FE0` FOREIGN KEY (`UIDpol`) REFERENCES `polPolicy` (`polUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jPolSecSec`
--

DROP TABLE IF EXISTS `jPolSecSec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jPolSecSec` (
  `UIDpol` bigint(20) NOT NULL,
  `sectionUIDsec` bigint(20) NOT NULL,
  UNIQUE KEY `sectionUIDsec` (`sectionUIDsec`),
  KEY `FK6AEEC1A37C8F2B7A` (`sectionUIDsec`),
  KEY `FK6AEEC1A384156FE0` (`UIDpol`),
  CONSTRAINT `FK6AEEC1A37C8F2B7A` FOREIGN KEY (`sectionUIDsec`) REFERENCES `secSection` (`secUID`),
  CONSTRAINT `FK6AEEC1A384156FE0` FOREIGN KEY (`UIDpol`) REFERENCES `polPolicy` (`polUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jPscMprMpr`
--

DROP TABLE IF EXISTS `jPscMprMpr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jPscMprMpr` (
  `UIDpsc` bigint(20) NOT NULL,
  `moneyProvisionUIDmpr` bigint(20) NOT NULL,
  UNIQUE KEY `moneyProvisionUIDmpr` (`moneyProvisionUIDmpr`),
  KEY `FK24C00B16ABCFDC05` (`moneyProvisionUIDmpr`),
  KEY `FK24C00B162BCAEBD1` (`UIDpsc`),
  CONSTRAINT `FK24C00B162BCAEBD1` FOREIGN KEY (`UIDpsc`) REFERENCES `pscPaymentSchedule` (`pscUID`),
  CONSTRAINT `FK24C00B16ABCFDC05` FOREIGN KEY (`moneyProvisionUIDmpr`) REFERENCES `mprMoneyProvision` (`mprUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jSecAslAsh`
--

DROP TABLE IF EXISTS `jSecAslAsh`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jSecAslAsh` (
  `UIDsec` bigint(20) NOT NULL,
  `assessmentSheetListUIDash` bigint(20) NOT NULL,
  `sheet_name` varchar(255) NOT NULL,
  PRIMARY KEY (`UIDsec`,`sheet_name`),
  UNIQUE KEY `assessmentSheetListUIDash` (`assessmentSheetListUIDash`),
  KEY `FK1EEA1C0373432C55` (`UIDsec`),
  KEY `FK1EEA1C0351048E0D` (`assessmentSheetListUIDash`),
  CONSTRAINT `FK1EEA1C0351048E0D` FOREIGN KEY (`assessmentSheetListUIDash`) REFERENCES `ashAssessmentSheet` (`ashUID`),
  CONSTRAINT `FK1EEA1C0373432C55` FOREIGN KEY (`UIDsec`) REFERENCES `secSection` (`secUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jSecCovCov`
--

DROP TABLE IF EXISTS `jSecCovCov`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jSecCovCov` (
  `UIDsec` bigint(20) NOT NULL,
  `coverageUIDcov` bigint(20) NOT NULL,
  UNIQUE KEY `coverageUIDcov` (`coverageUIDcov`),
  KEY `FK222002073DED8F7B` (`coverageUIDcov`),
  KEY `FK2220020773432C55` (`UIDsec`),
  CONSTRAINT `FK222002073DED8F7B` FOREIGN KEY (`coverageUIDcov`) REFERENCES `covCoverage` (`covUID`),
  CONSTRAINT `FK2220020773432C55` FOREIGN KEY (`UIDsec`) REFERENCES `secSection` (`secUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `liqChangeLog`
--

DROP TABLE IF EXISTS `liqChangeLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liqChangeLog` (
  `ID` varchar(255) NOT NULL,
  `AUTHOR` varchar(255) NOT NULL,
  `FILENAME` varchar(255) NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) NOT NULL,
  `MD5SUM` varchar(35) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `TAG` varchar(255) DEFAULT NULL,
  `LIQUIBASE` varchar(20) DEFAULT NULL,
  `CONTEXTS` varchar(255) DEFAULT NULL,
  `LABELS` varchar(255) DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `liqChangeLogLock`
--

DROP TABLE IF EXISTS `liqChangeLogLock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liqChangeLogLock` (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mprMoneyProvision`
--

DROP TABLE IF EXISTS `mprMoneyProvision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mprMoneyProvision` (
  `mprUID` bigint(20) NOT NULL,
  `mprAttribute` longtext,
  `mprCreatedDate` datetime DEFAULT NULL,
  `mprExternalSystemId` varchar(255) NOT NULL,
  `mprForeignSystemId` varchar(255) DEFAULT NULL,
  `mprLock` bit(1) DEFAULT NULL,
  `mprSerialVersion` bigint(20) NOT NULL,
  `mprUpdatedDate` datetime DEFAULT NULL,
  `mprAmount` decimal(19,2) DEFAULT NULL,
  `mprCurrency` varchar(255) DEFAULT NULL,
  `mprDescription` varchar(255) DEFAULT NULL,
  `mprFrequency` varchar(255) DEFAULT NULL,
  `mprNumber` int(11) NOT NULL,
  `mprPaymentId` varchar(255) DEFAULT NULL,
  `mprPaymentsEndDate` datetime DEFAULT NULL,
  `mprPaymentsStartDate` datetime DEFAULT NULL,
  `mprSaleId` varchar(255) DEFAULT NULL,
  `mprStatus` varchar(255) DEFAULT NULL,
  `mprPaymentMethodUIDpme` bigint(20) DEFAULT NULL,
  `mprCreatedBy` bigint(20) DEFAULT NULL,
  `mprUpdatedBy` bigint(20) DEFAULT NULL,
  `mprDay` int(11) DEFAULT NULL,
  PRIMARY KEY (`mprUID`),
  UNIQUE KEY `mprExternalSystemId` (`mprExternalSystemId`),
  KEY `FK407B25044212110A` (`mprPaymentMethodUIDpme`),
  CONSTRAINT `FK407B25044212110A` FOREIGN KEY (`mprPaymentMethodUIDpme`) REFERENCES `pmePaymentMethod` (`pmeUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notNote`
--

DROP TABLE IF EXISTS `notNote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notNote` (
  `notUID` bigint(20) NOT NULL,
  `notAttribute` longtext,
  `notCreatedDate` datetime DEFAULT NULL,
  `notExternalSystemId` varchar(255) NOT NULL,
  `notForeignSystemId` varchar(255) DEFAULT NULL,
  `notLock` bit(1) DEFAULT NULL,
  `notSerialVersion` bigint(20) NOT NULL,
  `notUpdatedDate` datetime DEFAULT NULL,
  `notBody` longtext,
  `notTitle` varchar(255) DEFAULT NULL,
  `notCreatedBy` bigint(20) DEFAULT NULL,
  `notUpdatedBy` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`notUID`),
  UNIQUE KEY `notExternalSystemId` (`notExternalSystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `parParty`
--

DROP TABLE IF EXISTS `parParty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parParty` (
  `parDSC` varchar(31) NOT NULL,
  `parUID` bigint(20) NOT NULL,
  `parAttribute` longtext,
  `parCreatedDate` datetime DEFAULT NULL,
  `parExternalSystemId` varchar(255) NOT NULL,
  `parForeignSystemId` varchar(255) DEFAULT NULL,
  `parLock` tinyint(1) NOT NULL,
  `parSerialVersion` bigint(20) NOT NULL,
  `parUpdatedDate` datetime DEFAULT NULL,
  `parCountry` varchar(255) DEFAULT NULL,
  `parCounty` varchar(255) DEFAULT NULL,
  `parFullAddress` varchar(255) DEFAULT NULL,
  `parLine1` varchar(255) DEFAULT NULL,
  `parLine2` varchar(255) DEFAULT NULL,
  `parLine3` varchar(255) DEFAULT NULL,
  `parLine4` varchar(255) DEFAULT NULL,
  `parLine5` varchar(255) DEFAULT NULL,
  `parPostcode` varchar(255) DEFAULT NULL,
  `parTown` varchar(255) DEFAULT NULL,
  `parEmailAddress` varchar(255) DEFAULT NULL,
  `parLegalName` varchar(255) DEFAULT NULL,
  `parMobilephoneNumber` varchar(255) DEFAULT NULL,
  `parPartyId` varchar(255) DEFAULT NULL,
  `parTelephoneNumber` varchar(255) DEFAULT NULL,
  `parOrganisationRegistrationNumber` varchar(255) DEFAULT NULL,
  `taxRegistrationNumber` varchar(255) DEFAULT NULL,
  `parClaimTelephoneNumber` varchar(255) DEFAULT NULL,
  `parContactName` varchar(255) DEFAULT NULL,
  `parDirectDebitIdentificationNumber` varchar(255) DEFAULT NULL,
  `parPaymentTelephoneNumber` varchar(255) DEFAULT NULL,
  `parPolicyEmailAddress` varchar(255) DEFAULT NULL,
  `parQuoteEmailAddress` varchar(255) DEFAULT NULL,
  `parQuoteTelephoneNumber` varchar(255) DEFAULT NULL,
  `parTradingName` varchar(255) DEFAULT NULL,
  `parDateOfBirth` datetime DEFAULT NULL,
  `parFirstName` varchar(255) DEFAULT NULL,
  `parOtherTitle` varchar(255) DEFAULT NULL,
  `parSurname` varchar(255) DEFAULT NULL,
  `parTitle` varchar(255) DEFAULT NULL,
  `parContactUIDpar` bigint(20) DEFAULT NULL,
  `parCreatedBy` bigint(20) DEFAULT NULL,
  `parUpdatedBy` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`parUID`),
  UNIQUE KEY `parExternalSystemId` (`parExternalSystemId`),
  KEY `FK44DDB6C518EEC24` (`parContactUIDpar`),
  KEY `legalName` (`parLegalName`),
  KEY `partyId` (`parPartyId`),
  KEY `emailAddress` (`parEmailAddress`),
  CONSTRAINT `FK44DDB6C518EEC24` FOREIGN KEY (`parContactUIDpar`) REFERENCES `parParty` (`parUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pceProductChangeEvent`
--

DROP TABLE IF EXISTS `pceProductChangeEvent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pceProductChangeEvent` (
  `pceUID` bigint(20) NOT NULL,
  `pceAttribute` longtext,
  `pceCreatedDate` datetime DEFAULT NULL,
  `pceExternalSystemId` varchar(255) NOT NULL,
  `pceForeignSystemId` varchar(255) DEFAULT NULL,
  `pceLock` tinyint(1) NOT NULL,
  `pceSerialVersion` bigint(20) NOT NULL,
  `pceUpdatedDate` datetime DEFAULT NULL,
  `pcePath` varchar(255) DEFAULT NULL,
  `pceType` varchar(255) DEFAULT NULL,
  `pceCreatedBy` bigint(20) DEFAULT NULL,
  `pceUpdatedBy` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`pceUID`),
  UNIQUE KEY `pceExternalSystemId` (`pceExternalSystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `phoPaymentHoliday`
--

DROP TABLE IF EXISTS `phoPaymentHoliday`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phoPaymentHoliday` (
  `phoUID` bigint(20) NOT NULL,
  `phoAttribute` longtext,
  `phoCreatedBy` bigint(20) DEFAULT NULL,
  `phoCreatedDate` datetime DEFAULT NULL,
  `phoExternalSystemId` varchar(255) NOT NULL,
  `phoForeignSystemId` varchar(255) DEFAULT NULL,
  `phoLock` bit(1) DEFAULT NULL,
  `phoSerialVersion` bigint(20) NOT NULL,
  `phoUpdatedBy` bigint(20) DEFAULT NULL,
  `phoUpdatedDate` datetime DEFAULT NULL,
  `phoEndDate` datetime DEFAULT NULL,
  `phoStartDate` datetime DEFAULT NULL,
  `phoPolicyUIDpol` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`phoUID`),
  UNIQUE KEY `phoExternalSystemId` (`phoExternalSystemId`),
  KEY `FKADB97CC9F9D31E89` (`phoPolicyUIDpol`),
  CONSTRAINT `FKADB97CC9F9D31E89` FOREIGN KEY (`phoPolicyUIDpol`) REFERENCES `polPolicy` (`polUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pmePaymentMethod`
--

DROP TABLE IF EXISTS `pmePaymentMethod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pmePaymentMethod` (
  `pmeDSC` varchar(31) NOT NULL,
  `pmeUID` bigint(20) NOT NULL,
  `pmeAttribute` longtext,
  `pmeCreatedDate` datetime DEFAULT NULL,
  `pmeExternalSystemId` varchar(255) NOT NULL,
  `pmeForeignSystemId` varchar(255) DEFAULT NULL,
  `pmeLock` bit(1) DEFAULT NULL,
  `pmeSerialVersion` bigint(20) NOT NULL,
  `pmeUpdatedDate` datetime DEFAULT NULL,
  `pmeIdentifier` varchar(255) DEFAULT NULL,
  `pmeAccountNumber` varchar(255) DEFAULT NULL,
  `pmeSortCode` varchar(255) DEFAULT NULL,
  `mprCardHoldersName` varchar(255) DEFAULT NULL,
  `mprCardNumber` varchar(255) DEFAULT NULL,
  `mprExpiryDate` datetime DEFAULT NULL,
  `mprIssueNumber` varchar(255) DEFAULT NULL,
  `mprIssuer` varchar(255) DEFAULT NULL,
  `mprSecurityCode` varchar(255) DEFAULT NULL,
  `mprStartDate` datetime DEFAULT NULL,
  `pmeCreatedBy` bigint(20) DEFAULT NULL,
  `pmeUpdatedBy` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`pmeUID`),
  UNIQUE KEY `pmeExternalSystemId` (`pmeExternalSystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `polPolicy`
--

DROP TABLE IF EXISTS `polPolicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `polPolicy` (
  `polUID` bigint(20) NOT NULL,
  `polAttribute` longtext,
  `polCreatedDate` datetime DEFAULT NULL,
  `polExternalSystemId` varchar(255) NOT NULL,
  `polForeignSystemId` varchar(255) DEFAULT NULL,
  `polLock` tinyint(1) NOT NULL,
  `polSerialVersion` bigint(20) NOT NULL,
  `polUpdatedDate` datetime DEFAULT NULL,
  `polAggregator` tinyint(1) NOT NULL,
  `polAllowable` longtext,
  `polBaseCurrency` varchar(255) DEFAULT NULL,
  `polClause` longtext,
  `polException` longtext,
  `polExcess` longtext,
  `polExpiryDate` datetime DEFAULT NULL,
  `polId` varchar(255) DEFAULT NULL,
  `polInceptionDate` datetime DEFAULT NULL,
  `polOwningUser` bigint(20) DEFAULT NULL,
  `polPageVisit` longtext,
  `polPaymentDetails` longtext,
  `polPaymentHistory` longtext,
  `polPaymentOption` longtext,
  `polPolicyLink` longtext,
  `polPolicyNumber` varchar(255) DEFAULT NULL,
  `polProductName` varchar(255) DEFAULT NULL,
  `polProductTypeId` varchar(255) DEFAULT NULL,
  `polQuotationDate` datetime DEFAULT NULL,
  `polQuotationExpiryDate` datetime DEFAULT NULL,
  `polQuotationNumber` varchar(255) DEFAULT NULL,
  `polStatus` varchar(255) DEFAULT NULL,
  `polTestCase` tinyint(1) NOT NULL,
  `polUserSaved` tinyint(1) NOT NULL,
  `polVersionEffectiveDate` datetime DEFAULT NULL,
  `polWording` longtext,
  `polBrokerUIDpar` bigint(20) DEFAULT NULL,
  `polPolicyHolderUIDpar` bigint(20) DEFAULT NULL,
  `polProposerUIDpar` bigint(20) DEFAULT NULL,
  `polPaymentDetailsUIDpsc` bigint(20) DEFAULT NULL,
  `polCreatedBy` bigint(20) DEFAULT NULL,
  `polUpdatedBy` bigint(20) DEFAULT NULL,
  `polMtaIndex` bigint(20) DEFAULT NULL,
  `polRenewalIndex` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`polUID`),
  UNIQUE KEY `polExternalSystemId` (`polExternalSystemId`),
  KEY `FKC88695DFF97558C1` (`polBrokerUIDpar`),
  KEY `FKC88695DFD76D8BF9` (`polProposerUIDpar`),
  KEY `FKC88695DF3FFA3FE3` (`polPolicyHolderUIDpar`),
  KEY `status` (`polStatus`),
  KEY `owningUser` (`polOwningUser`),
  KEY `FKC88695DF9F98CA5F` (`polPaymentDetailsUIDpsc`),
  CONSTRAINT `FKC88695DF3FFA3FE3` FOREIGN KEY (`polPolicyHolderUIDpar`) REFERENCES `parParty` (`parUID`),
  CONSTRAINT `FKC88695DF9F98CA5F` FOREIGN KEY (`polPaymentDetailsUIDpsc`) REFERENCES `pscPaymentSchedule` (`pscUID`),
  CONSTRAINT `FKC88695DFD76D8BF9` FOREIGN KEY (`polProposerUIDpar`) REFERENCES `parParty` (`parUID`),
  CONSTRAINT `FKC88695DFF97558C1` FOREIGN KEY (`polBrokerUIDpar`) REFERENCES `parParty` (`parUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `prePaymentRecord`
--

DROP TABLE IF EXISTS `prePaymentRecord`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prePaymentRecord` (
  `preUID` bigint(20) NOT NULL,
  `preAttribute` longtext,
  `preCreatedDate` datetime DEFAULT NULL,
  `preExternalSystemId` varchar(255) NOT NULL,
  `preForeignSystemId` varchar(255) DEFAULT NULL,
  `preLock` bit(1) DEFAULT NULL,
  `preSerialVersion` bigint(20) NOT NULL,
  `preUpdatedDate` datetime DEFAULT NULL,
  `preAmount` decimal(19,2) DEFAULT NULL,
  `preCurrency` varchar(255) DEFAULT NULL,
  `preDate` datetime DEFAULT NULL,
  `preDescription` varchar(255) DEFAULT NULL,
  `preMethodIdentifier` varchar(255) DEFAULT NULL,
  `preTransactionReference` varchar(255) DEFAULT NULL,
  `preType` varchar(255) DEFAULT NULL,
  `preCreatedBy` bigint(20) DEFAULT NULL,
  `preUpdatedBy` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`preUID`),
  UNIQUE KEY `preExternalSystemId` (`preExternalSystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pscPaymentSchedule`
--

DROP TABLE IF EXISTS `pscPaymentSchedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pscPaymentSchedule` (
  `pscUID` bigint(20) NOT NULL,
  `pscAttribute` longtext,
  `pscCreatedDate` datetime DEFAULT NULL,
  `pscExternalSystemId` varchar(255) NOT NULL,
  `pscForeignSystemId` varchar(255) DEFAULT NULL,
  `pscLock` bit(1) DEFAULT NULL,
  `pscSerialVersion` bigint(20) NOT NULL,
  `pscUpdatedDate` datetime DEFAULT NULL,
  `pscDescription` varchar(255) DEFAULT NULL,
  `pscCreatedBy` bigint(20) DEFAULT NULL,
  `pscUpdatedBy` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`pscUID`),
  UNIQUE KEY `pscExternalSystemId` (`pscExternalSystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `secSection`
--

DROP TABLE IF EXISTS `secSection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `secSection` (
  `secUID` bigint(20) NOT NULL,
  `secAttribute` longtext,
  `secCreatedDate` datetime DEFAULT NULL,
  `secExternalSystemId` varchar(255) NOT NULL,
  `secForeignSystemId` varchar(255) DEFAULT NULL,
  `secLock` tinyint(1) NOT NULL,
  `secSerialVersion` bigint(20) NOT NULL,
  `secUpdatedDate` datetime DEFAULT NULL,
  `secAssetId` longtext,
  `secClause` longtext,
  `secExcessId` longtext,
  `secExcluded` varchar(255) DEFAULT NULL,
  `secId` varchar(255) DEFAULT NULL,
  `secIncluded` varchar(255) DEFAULT NULL,
  `secSectionTypeId` varchar(255) DEFAULT NULL,
  `secUninsuredAssetId` longtext,
  `secWording` longtext,
  `secCreatedBy` bigint(20) DEFAULT NULL,
  `secUpdatedBy` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`secUID`),
  UNIQUE KEY `secExternalSystemId` (`secExternalSystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sgeSequenceGenerator`
--

DROP TABLE IF EXISTS `sgeSequenceGenerator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sgeSequenceGenerator` (
  `sequence_name` varchar(255) NOT NULL,
  `next_val` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`sequence_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `srrServiceRequestRecord`
--

DROP TABLE IF EXISTS `srrServiceRequestRecord`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `srrServiceRequestRecord` (
  `srrUID` bigint(20) NOT NULL,
  `srrAttribute` longtext,
  `srrCreatedDate` datetime DEFAULT NULL,
  `srrExternalSystemId` varchar(255) NOT NULL,
  `srrForeignSystemId` varchar(255) DEFAULT NULL,
  `srrLock` tinyint(1) NOT NULL,
  `srrSerialVersion` bigint(20) NOT NULL,
  `srrUpdatedDate` datetime DEFAULT NULL,
  `srrCommand` varchar(255) DEFAULT NULL,
  `srrEntryTimestamp` datetime DEFAULT NULL,
  `srrExitTimestamp` datetime DEFAULT NULL,
  `srrExternalPolicyId` varchar(255) DEFAULT NULL,
  `srrProduct` varchar(255) DEFAULT NULL,
  `srrRequest` longtext,
  `srrResponse` longtext,
  `srrCreatedBy` bigint(20) DEFAULT NULL,
  `srrUpdatedBy` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`srrUID`),
  UNIQUE KEY `srrExternalSystemId` (`srrExternalSystemId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ukeUniqueKey`
--

DROP TABLE IF EXISTS `ukeUniqueKey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ukeUniqueKey` (
  `ukeId` varchar(255) NOT NULL,
  `ukeValue` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ukeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-20 22:06:49

--
-- Dumping data for table `liqChangeLog`
--

LOCK TABLES `liqChangeLog` WRITE;
/*!40000 ALTER TABLE `liqChangeLog` DISABLE KEYS */;
INSERT INTO `liqChangeLog` VALUES ('OU-715 Add pceProductChangeEvent table','richard','db.changelog.xml','2017-10-20 22:04:50',1,'EXECUTED','7:77ddf9897c147123e194eebb7c279417','sql','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('YEL-32 Add payment schedule tables','richard','db.changelog.xml','2017-10-20 22:04:50',2,'EXECUTED','7:90a0cb415a7660e4dac31eb5ff0b255b','sql','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('OU-756 Rename DocumentContent content column','richard','db.changelog.xml','2017-10-20 22:04:50',3,'EXECUTED','7:25fea51897f8c7b30a0f38a9c1577f79','sql','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('OU-756 Add dcoProductTypeId column','richard','db.changelog.xml','2017-10-20 22:04:51',4,'EXECUTED','7:83754e0696eeb6dd44ced0f7e5685a30','sql','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('OU-756 Add fsrForeignSystemReference table','richard','db.changelog.xml','2017-10-20 22:04:51',5,'EXECUTED','7:627bbeb75614c42bf00dd55a304d7c63','sql','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('OU-756 Add jDcoFsrFsr table','richard','db.changelog.xml','2017-10-20 22:04:51',6,'EXECUTED','7:f5ec1d9cb3edea81b75ca4c4a632ea9b','sql','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('OU-764 Add notNote table','richard','db.changelog.xml','2017-10-20 22:04:51',7,'EXECUTED','7:09de8de8e1a02f691c61e80d34507b47','sql','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('OU-764 Add jPolNotNot table','richard','db.changelog.xml','2017-10-20 22:04:51',8,'EXECUTED','7:5e552f0db0e78edeff5973bba21e0c00','sql','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('OU-775 Add createdBy and updatedBy audit columns','richard','db.changelog.xml','2017-10-20 22:04:52',9,'EXECUTED','7:adbcdff04006db9cba2a03941d628b3d','sql','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('OU-796 Add covCoverage table','richard','db.changelog.xml','2017-10-20 22:04:52',10,'EXECUTED','7:609b1b61d3dd9d42c4093ba505f8c05b','sql','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('OU-796 Add join table for Policy Coverage','richard','db.changelog.xml','2017-10-20 22:04:52',11,'EXECUTED','7:2f5c071c6409a768efcb39c2424c85ae','sql','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('OU-796 Add join table for Section Coverage','richard','db.changelog.xml','2017-10-20 22:04:52',12,'EXECUTED','7:4c264b54584f0c5a141a11fd54fde590','sql','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('OU-758 Add assAsset table','richard','db.changelog.xml','2017-10-20 22:04:52',13,'EXECUTED','7:5a1d8e88595d2febf0f88288ff9e9d0a','sql','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('OU-758 Add jAssAssAss table','richard','db.changelog.xml','2017-10-20 22:04:52',14,'EXECUTED','7:ffc1af34dd64b319ad73b0d8291e389b','sql','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('OU-758 Add jPolAssAss table','richard','db.changelog.xml','2017-10-20 22:04:52',15,'EXECUTED','7:9a32a41b355cd8b1d678945a0254fe90','sql','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('OU-825 Change note body column type','richard','db.changelog.xml','2017-10-20 22:04:52',16,'EXECUTED','7:a42ccf7171bc105f84d39efdb80b3a65','sql','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('OU-825 Change document descrption column type','richard','db.changelog.xml','2017-10-20 22:04:52',17,'EXECUTED','7:89ee43f12700ae81b622c598a0492cab','sql','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('OU-823 Add renewal and MTA indexes to Policy','richard','db.changelog.xml','2017-10-20 22:04:52',18,'EXECUTED','7:f1c0476e6dcdc891bf02b49c1515b944','sql','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('OU-854 Add phoPaymentHoliday table','richard','db.changelog.xml','2017-10-20 22:04:52',19,'EXECUTED','7:a980141212abb93e12ab84f4f9ec2ce2','sql','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('OU-862 Add mprDay column','richard','db.changelog.xml','2017-10-20 22:04:52',20,'EXECUTED','7:47d39565053dc3b5c0f6685bf5dd2a41','sql','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('YEL-32 Migrate payment date from XML to relational model','mat','db.changelog.xml','2017-10-20 22:04:56',21,'EXECUTED','7:64ae2c6288ef2bbd1174f53b1079d4a0','executeCommand; executeCommand; executeCommand','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('OU-796 Migrate policy coverage date from XML to relational model','richard','db.changelog.xml','2017-10-20 22:04:57',22,'EXECUTED','7:64ae2c6288ef2bbd1174f53b1079d4a0','executeCommand; executeCommand; executeCommand','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('OU-796 Migrate section coverage date from XML to relational model','richard','db.changelog.xml','2017-10-20 22:05:01',23,'EXECUTED','7:64ae2c6288ef2bbd1174f53b1079d4a0','executeCommand; executeCommand; executeCommand','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('OU-796 Drop redundant columns','richard','db.changelog.xml','2017-10-20 22:05:01',24,'EXECUTED','7:7b17fd815a062ed7d5e797af9b36a712','sql','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('OU-758 Migrate asset date from XML to relational model','richard','db.changelog.xml','2017-10-20 22:05:09',25,'EXECUTED','7:64ae2c6288ef2bbd1174f53b1079d4a0','executeCommand; executeCommand; executeCommand','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('OU-758 Drop redundant columns','richard','db.changelog.xml','2017-10-20 22:05:09',26,'EXECUTED','7:ea2a2ff2daeefb5152d54acf16bfc76f','sql','',NULL,'3.5.3',NULL,NULL,'8533490648');
INSERT INTO `liqChangeLog` VALUES ('OU-877 Correct name of paymentMethodUIDpme column to mprPaymentMethodUIDpme','richard','db.changelog.xml','2017-10-23 11:33:33',27,'EXECUTED','7:0c9cf669c662c535d6d228c9044c6cb4','sql','',NULL,'3.5.3',NULL,NULL,'8754813160');
/*!40000 ALTER TABLE `liqChangeLog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `liqChangeLogLock`
--

LOCK TABLES `liqChangeLogLock` WRITE;
/*!40000 ALTER TABLE `liqChangeLogLock` DISABLE KEYS */;
INSERT INTO `liqChangeLogLock` VALUES (1,'\0',NULL,NULL);
/*!40000 ALTER TABLE `liqChangeLogLock` ENABLE KEYS */;
UNLOCK TABLES;


