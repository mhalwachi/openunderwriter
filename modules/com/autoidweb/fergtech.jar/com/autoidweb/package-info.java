/**
 * <b>XML web service (MTOM) that produces auto-insurance card output.</b> (<i>AutoIDweb</i>)
 * 
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "https://www.autoidweb.com/webservices/", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package com.autoidweb.webservices;
