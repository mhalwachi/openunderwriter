
package com.autoidweb.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CompanyTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DateTimeStamp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SecurityToken" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AttachManifest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Files2Proc" type="{https://www.autoidweb.com/webservices/}ArrayOfBase64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "companyTitle",
    "dateTimeStamp",
    "securityToken",
    "attachManifest",
    "files2Proc"
})
@XmlRootElement(name = "ProcessAIWrequest")
public class ProcessAIWrequest {

    @XmlElement(name = "CompanyTitle")
    protected String companyTitle;
    @XmlElement(name = "DateTimeStamp")
    protected String dateTimeStamp;
    @XmlElement(name = "SecurityToken")
    protected String securityToken;
    @XmlElement(name = "AttachManifest")
    protected String attachManifest;
    @XmlElement(name = "Files2Proc")
    protected ArrayOfBase64Binary files2Proc;

    /**
     * Gets the value of the companyTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyTitle() {
        return companyTitle;
    }

    /**
     * Sets the value of the companyTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyTitle(String value) {
        this.companyTitle = value;
    }

    /**
     * Gets the value of the dateTimeStamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateTimeStamp() {
        return dateTimeStamp;
    }

    /**
     * Sets the value of the dateTimeStamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateTimeStamp(String value) {
        this.dateTimeStamp = value;
    }

    /**
     * Gets the value of the securityToken property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecurityToken() {
        return securityToken;
    }

    /**
     * Sets the value of the securityToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecurityToken(String value) {
        this.securityToken = value;
    }

    /**
     * Gets the value of the attachManifest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttachManifest() {
        return attachManifest;
    }

    /**
     * Sets the value of the attachManifest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttachManifest(String value) {
        this.attachManifest = value;
    }

    /**
     * Gets the value of the files2Proc property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfBase64Binary }
     *     
     */
    public ArrayOfBase64Binary getFiles2Proc() {
        return files2Proc;
    }

    /**
     * Sets the value of the files2Proc property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfBase64Binary }
     *     
     */
    public void setFiles2Proc(ArrayOfBase64Binary value) {
        this.files2Proc = value;
    }

}
