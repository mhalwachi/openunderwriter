
package com.autoidweb.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProcessAIWrequestResult" type="{https://www.autoidweb.com/webservices/}ArrayOfBase64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "processAIWrequestResult"
})
@XmlRootElement(name = "ProcessAIWrequestResponse")
public class ProcessAIWrequestResponse {

    @XmlElement(name = "ProcessAIWrequestResult")
    protected ArrayOfBase64Binary processAIWrequestResult;

    /**
     * Gets the value of the processAIWrequestResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfBase64Binary }
     *     
     */
    public ArrayOfBase64Binary getProcessAIWrequestResult() {
        return processAIWrequestResult;
    }

    /**
     * Sets the value of the processAIWrequestResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfBase64Binary }
     *     
     */
    public void setProcessAIWrequestResult(ArrayOfBase64Binary value) {
        this.processAIWrequestResult = value;
    }

}
