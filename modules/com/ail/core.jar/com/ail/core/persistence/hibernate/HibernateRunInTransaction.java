/* Copyright Applied Industrial Logic Limited 2015. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.ail.core.persistence.hibernate;

import java.lang.reflect.InvocationTargetException;

import javax.naming.InitialContext;
import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.hibernate.FlushMode;
import org.hibernate.Session;

import com.ail.core.CoreContext;
import com.ail.core.persistence.OutsideTransactionContext;

public abstract class HibernateRunInTransaction<T> {
    private T result;

    public abstract T run() throws Throwable;

    public T result() {
        return result;
    }

    public HibernateRunInTransaction<T> invoke() throws Throwable {
        return invoke(false);
    }

    public HibernateRunInTransaction<T> invoke(boolean readonly) throws Throwable {
        OutsideTransactionContext.initialise();
        UserTransaction userTransaction = null;
        Session session = null;

        try {
            userTransaction = beginUserTransaction();
            session = HibernateSessionBuilder.getSessionFactory().openSession();

            if (readonly) {
                session.setFlushMode(FlushMode.MANUAL);
                session.setDefaultReadOnly(true);
            }

            session.getTransaction().begin();

            result = this.run();

            commitTransaction(session, userTransaction);

            OutsideTransactionContext.executePostCommitCommands();
        } catch (ForceSilentRollbackError e) {
            rollbackTransaction(session, userTransaction);
        } catch (Throwable t) {
            rollbackTransaction(session, userTransaction);
            if (!(t.getCause() instanceof ForceSilentRollbackError)) {
                if (t instanceof InvocationTargetException) {
                    throw ((InvocationTargetException) t).getTargetException();
                } else {
                    throw t;
                }
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
            OutsideTransactionContext.destroy();
        }

        return this;
    }

    private UserTransaction beginUserTransaction() {
        UserTransaction userTransaction = null;
        try {
            userTransaction = (UserTransaction) InitialContext.doLookup("java:jboss/UserTransaction");
            userTransaction.begin();
        } catch (Exception e) {
            CoreContext.getCoreProxy().logWarning("Unable to get UserTransaction: " + e.getMessage());
        }

        return userTransaction;
    }

    private void commitTransaction(Session session, UserTransaction userTransaction) throws Exception {
        if (userTransaction != null) {
            userTransaction.commit();
        } else {
            session.getTransaction().commit();
        }
    }

    private void rollbackTransaction(Session session, UserTransaction userTransaction) throws SystemException {
        if (userTransaction != null && userTransaction.getStatus() == Status.STATUS_ACTIVE) {
            userTransaction.rollback();
        } else if (session != null && session.getTransaction() != null && session.getTransaction().isActive()) {
            session.getTransaction().rollback();
        }
    }
}
