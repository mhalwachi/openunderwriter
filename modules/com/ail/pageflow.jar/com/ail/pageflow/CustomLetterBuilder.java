package com.ail.pageflow;

import static com.ail.core.CoreContext.getRequestWrapper;
import static java.util.regex.Pattern.compile;

import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.ail.core.Attribute;
import com.ail.core.BaseException;
import com.ail.core.HasDocuments;
import com.ail.core.Type;
import com.ail.core.configure.Parameter;
import com.ail.core.context.RequestWrapper;
import com.ail.party.HasPartyRoles;
import com.ail.party.PartyRole;

public class CustomLetterBuilder extends PageContainer {
    private static final String DEFAULT_CUSTOM_CONTENT_ATTRIBUTE = "customLetterBuilderContent";
    private static final String DEFAULT_SIGNATURE_SERVICE_URL = "";
    private static final String REQUEST_PROPERTY_NAME = "selectedDocumentType";
    private static final String SELECTED_RECIPIENT_ID_ATTR_NAME = "SELECTED_RECIPIENT_ID_ATTR_NAME";
    private static final String SELECTED_DOCUMENT_TYPE_ATTR_NAME = "SELECTED_DOCUMENT_TYPE_ATTR_NAME";

    private static final long serialVersionUID = 7575333161919813599L;

    private static final Pattern MATCH_ALL = compile(".*");

    private String validDocumentTypes = ".*";
    private Pattern validDocumentTypesPattern = MATCH_ALL;

    private String validRecipientTypes = ".*";
    private Pattern validRecipientTypesPattern = MATCH_ALL;

    private String signatureServiceUrl = DEFAULT_SIGNATURE_SERVICE_URL;

    private String customContentBindingAttributeName = DEFAULT_CUSTOM_CONTENT_ATTRIBUTE;

    private String saveDestinationPageId;

    private String requestPropertyName = REQUEST_PROPERTY_NAME;

    public CustomLetterBuilder() {
        super();
    }

    public List<String> fetchValidDocumentTypes() {
        return PageFlowContext.getCoreProxy().getGroup("DocumentTemplates").getParameter().stream().filter(p -> validDocumentTypesPattern.matcher(p.getName()).matches()).map(Parameter::getName)
                .collect(Collectors.toList());
    }

    // Support methods for document types options and selection.

    public String getValidDocumentTypes() {
        return validDocumentTypes;
    }

    public void setValidDocumentTypes(String validDocumentTypes) {
        this.validDocumentTypes = validDocumentTypes;
        this.validDocumentTypesPattern = compile(validDocumentTypes);
    }

    public String getSelectedDocumentType() {
        return (String) getRequestWrapper().getServletRequest().getAttribute(SELECTED_DOCUMENT_TYPE_ATTR_NAME);

    }

    public void setSelectedDocumentType(String selectedDocumentType) {
        getRequestWrapper().getServletRequest().setAttribute(SELECTED_DOCUMENT_TYPE_ATTR_NAME, selectedDocumentType);;
    }

    public String getSelectedDocumentTypeFieldId() {
        return encodeId(getId() + "documentTypeId");
    }

    // Support methods for custom content entry and update.

    public String getCustomContent(Type model) {
        Attribute target = model.xpathGet(customeContentAttributeXPath(), (Attribute) null, Attribute.class);
        return (target != null) ? target.getValue() : null;
    }

    public void setCustomContent(Type model, String customContent) {
        Attribute target = model.xpathGet(customeContentAttributeXPath(), null, Attribute.class);
        if (target != null) {
            target.setValue(customContent);
        } else {
            model.addAttribute(new Attribute(customContentBindingAttributeName, customContent, "note"));
        }
    }

    private String customeContentAttributeXPath() {
        return "attribute[id='" + customContentBindingAttributeName + "']";
    }

    public String getCustomContentFieldId() {
        return encodeId(getId() + "customContentId");
    }

    public String getCustomContentBindingAttributeName() {
        return customContentBindingAttributeName;
    }

    public void setCustomContentBindingAttributeName(String customContentBindingAttributeName) {
        this.customContentBindingAttributeName = customContentBindingAttributeName;
    }

    // Support methods for recipient options and selection.

    public List<PartyRole> fetchValidRecipients(HasPartyRoles target) {
        return target.fetchPartyRolesForRoleTypes(validRecipientTypesPattern);
    }

    public String getValidRecipientTypes() {
        return validRecipientTypes;
    }

    public void setValidRecipientTypes(String validRecipientTypes) {
        this.validRecipientTypes = validRecipientTypes;
        this.validRecipientTypesPattern = compile(validRecipientTypes);
    }

    public String getSelectedRecipientFieldId() {
        return encodeId(getId() + "recipientTypeId");
    }

    public String getSelectedRecipientId() {
        return (String) getRequestWrapper().getServletRequest().getAttribute(SELECTED_RECIPIENT_ID_ATTR_NAME);
    }

    public void setSelectedRecipientId(String selectedRecipientId) {
        getRequestWrapper().getServletRequest().setAttribute(SELECTED_RECIPIENT_ID_ATTR_NAME, selectedRecipientId);
    }

    // Support methods for destination page

    public String getSaveDestinationPageId() {
        return saveDestinationPageId;
    }

    public void setSaveDestinationPageId(String saveDestinationPageId) {
        this.saveDestinationPageId = saveDestinationPageId;
    }

    public String getRequestPropertyName() {
        return requestPropertyName;
    }

    public String getSignatureServiceUrl() {
        return signatureServiceUrl;
    }

    public void setSignatureServiceUrl(String signatureServiceUrl) {
        this.signatureServiceUrl = signatureServiceUrl;
    }

    public void setRequestPropertyName(String requestPropertyName) {
        this.requestPropertyName = requestPropertyName;
    }

    @Override
    public Type applyRequestValues(Type model) {
        String paramName;
        String paramValue;

        super.applyRequestValues(model);

        RequestWrapper request = getRequestWrapper();

        Type boundModel = (Type) fetchBoundObject(model, model);

        if (!(boundModel instanceof HasDocuments)) {
            return model;
        }

        paramName = getSelectedDocumentTypeFieldId();
        paramValue = request.getParameter(paramName);
        setSelectedDocumentType(paramValue);

        paramName = getSelectedRecipientFieldId();
        paramValue = request.getParameter(paramName);
        setSelectedRecipientId(paramValue);

        paramName = getCustomContentFieldId();
        paramValue = request.getParameter(paramName);
        if (paramValue != null) {
            setCustomContent(boundModel, paramValue);
        }

        return model;
    }

    @Override
    public Type processActions(Type model) throws BaseException {
        PageFlowContext.getSessionTemp().addAttribute(new Attribute(requestPropertyName, getSelectedDocumentType(), "string"));

        return super.processActions(model);
    }

    @Override
    public Type renderResponse(Type model) throws IllegalStateException, IOException {
        return executeTemplateCommand("CustomLetterBuilder", model);
    }
}
