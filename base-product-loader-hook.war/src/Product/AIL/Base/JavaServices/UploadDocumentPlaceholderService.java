
/* Copyright Applied Industrial Logic Limited 2018. All rights Reserved */

import static java.net.HttpURLConnection.HTTP_OK;

import com.ail.core.BaseException;
import com.ail.core.HasDocuments;
import com.ail.core.PreconditionException;
import com.ail.core.RestfulServiceInvoker;
import com.ail.core.RestfulServiceReturn;
import com.ail.core.document.DocumentPlaceholder;
import com.ail.core.language.I18N;
import com.ail.pageflow.ExecutePageActionService.ExecutePageActionArgument;
import com.ail.pageflow.PageFlowContext;

public class UploadDocumentPlaceholderService extends RestfulServiceInvoker {

	public static void invoke(ExecutePageActionArgument args) throws BaseException {
		new UploadDocumentPlaceholderService().invoke(Argument.class);
	}

	public RestfulServiceReturn service(Argument argument) throws PreconditionException {
		
		if (PageFlowContext.getPolicy() == null) {
			throw new PreconditionException("PageFlowContext.getPolicy() == null");
		}

		DocumentPlaceholder documentPlaceholder = addDocumentPlaceholderToPolicy(argument);

		return new Return(HTTP_OK, documentPlaceholder.getExternalSystemId());
	}

	private DocumentPlaceholder addDocumentPlaceholderToPolicy(Argument arg) {

	    DocumentPlaceholder docPlaceholder = new DocumentPlaceholder(arg.documentType, I18N.i18n(arg.documentType));

		if (arg.title != null) {
		    docPlaceholder.setTitle(arg.title);
		}

		docPlaceholder.setDescription(arg.description);

		if (arg.target != null && arg.target.length() > 0) {
		    HasDocuments hasDocuments = (HasDocuments)PageFlowContext.getPolicy().xpathGet(arg.target);
		    hasDocuments.getDocumentPlaceholder().add(docPlaceholder);
		} else {
		    PageFlowContext.getPolicy().getDocumentPlaceholder().add(docPlaceholder);
		}

		PageFlowContext.getCoreProxy().flush();

		return docPlaceholder;
	}

	public static class Argument {
		String documentType;
		String title;
		String description;
		String target;

		public Argument() {
		}

		public Argument(String documentType, String title, String description, String target) {
			this.documentType = documentType;
			this.title = title;
			this.description = description;
			this.target = target;
		}
	}

	public static class Return extends RestfulServiceReturn {
		String documentPlaceholderId;

		public Return(int status, String documentPlaceholderId) {
			super(status);
			this.documentPlaceholderId = documentPlaceholderId;
		}
	}
}